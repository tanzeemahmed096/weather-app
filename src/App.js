import React, { Component } from "react";
import "./App.css";
import WeatherCard from "./components/WeatherCard";
import SelectCity from "./components/SelectCity";

class App extends Component {
  state = {
    city: "Bengaluru",
    weeklyWeatherData: [],
  };

  componentDidMount() {
    fetch(
      "https://api.openweathermap.org/data/2.5/forecast?q=Bengaluru&appid=326b8891620d0f22725fb855480e8ffa&units=metric"
    )
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          value: "Search for a city...",
          city: "Bengaluru",
          weeklyWeatherData: data.list,
        });
      })
      .catch((err) => console.log(err));
  }

  updateState = (cityName) => {
    fetch(
      `https://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=326b8891620d0f22725fb855480e8ffa&units=metric`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          city: cityName,
          weeklyWeatherData: data.list || [],
        });
      })
      .catch((err) => console.log(err));
  }

  render() {
    const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const date = new Date();
    const getDay = date.getDay();

    return (
      <div className="App">
      <SelectCity value={this.state.value} updateState={this.updateState}/>
      <div className="main">
      {this.state.weeklyWeatherData.length === 0 ? <h1>City Not Found...</h1> : this.state.weeklyWeatherData.slice(0,7).map((dailyData, idx) => {
        return <WeatherCard key={idx} day={days[(getDay + idx) % days.length]} dailyData={dailyData}/>
      })}
      </div>
      </div>
    );
  }
}

export default App;
