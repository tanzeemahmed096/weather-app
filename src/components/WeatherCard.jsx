import React, { Component } from "react";
import "../css/weatherCard.css"

class WeatherCard extends Component {
  render() {
    const { dailyData } = this.props;
    return (
      <div className="card">
        <p style={{marginTop: "0.4rem", marginBottom: "0", fontSize: "15px", fontWeight: "600"}}>{ this.props.day }</p>
        <img src={`https://openweathermap.org/img/wn/${dailyData.weather[0].icon}@2x.png`} className="weather-icon" alt="weather icon" />
        <div className="temp">
          <p className="temp-data">{ Math.round(dailyData.main.temp_max) }°</p>
          <p className="temp-data">{ Math.round(dailyData.main.temp_min) }°</p>
        </div>
      </div>
    );
  }
}

export default WeatherCard;
