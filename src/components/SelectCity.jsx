import React, { Component } from "react";
import "../css/selectCity.css";

class SelectCity extends Component {
  state = {
    value: "Search for a city",
  };

  handleChange = (e) => {
    this.setState({ value: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.updateState(this.state.value);
  };

  render() {
    return (
      <div className="cities">
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="city"
            className="inputBox"
            placeholder="Search for a city..."
            onChange={this.handleChange}
          />
          <input type="submit" className="search-btn" value="Search" />
        </form>
      </div>
    );
  }
}

export default SelectCity;
